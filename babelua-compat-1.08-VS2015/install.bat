@echo off
cd /d %~dp0

rem set "vs12=HKEY_CURRENT_USER\SOFTWARE\Microsoft\VisualStudio\12.0\ExtensionManager\EnabledExtensions"
set "vs14=HKEY_CURRENT_USER\SOFTWARE\Microsoft\VisualStudio\14.0\ExtensionManager\EnabledExtensions"

rem set bbl12=
rem for /f "tokens=2*" %%i in ('reg query "%vs12%" /v "ef2b9b1b-2d16-4088-95d9-904637d77b19,3.2.2.0" 2^>nul') do set "bbl12=%%j"

set bbl14=
for /f "tokens=2*" %%i in ('reg query "%vs14%" /v "ef2b9b1b-2d16-4088-95d9-904637d77b19,3.2.2.0" 2^>nul') do set "bbl14=%%j"

set patch_installed=
rem if defined bbl12 fc "%bbl12%\LuaInject.dll" LuaInject.dll >nul && set patch_installed=1
if defined bbl14 fc "%bbl14%\LuaInject.dll" LuaInject.dll >nul && set patch_installed=1

if defined patch_installed echo The patch already installed. && goto :L_end

mkdir backup 2>nul

rem if defined bbl12 echo backup files for vs2013... && copy "%bbl12%\LuaInject.dll" .\backup\LuaInject-2013.dll
if defined bbl14 echo backup files for vs2015... && copy "%bbl14%\LuaInject.dll" .\backup\LuaInject-2015.dll

set counter=0
rem if defined bbl12 echo install patch for vs2013-BabeLua... && copy /y LuaInject.dll %bbl12% && copy /y lua5.x-all.bin %bbl12% && set /a counter=%counter%+1 && echo succeed.
if defined bbl14 echo install patch for vs2015-BabeLua... && copy /y LuaInject.dll %bbl14% && copy /y lua5.x-all.bin %bbl14% && set /a counter=%counter%+1 && echo succeed.

if %counter%==0 echo No BabeLua-3.2.2.0 plugin installed for Visual Studio 2015! && pause

:L_end
ping /n 3 127.0.1>nul

goto :eof
