@echo off

echo 同步策划配置到demo和开发资源目录
copy /y client\*.csv ..\..\Arts\demo\游戏\configs\ 2>nul
copy /y client\*.csv ..\..\trunk\TDGame\Resources\configs 2>nul

echo 同步策划地图到开发资源目录
copy /y ..\..\Arts\demo\游戏\maps\*.xmlze ..\..\trunk\TDGame\Resources\maps\ 2>nul

echo 同步策划地图缩略图
copy /y ..\..\Arts\demo\游戏\images\maps\*.png ..\..\trunk\TDGame\Resources\images\maps\ 2>nul

echo 同步策划地图道路块
xcopy /e /q /y ..\..\Arts\demo\游戏\images\road\*.* ..\..\trunk\TDGame\Resources\images\road\ 2>nul


echo 同步策划配置到jason本地demo目录
copy /y client\*.csv F:\美术资源\demo\游戏\configs\ 2>nul

echo 拷贝二进制到demo目录
copy /y ..\..\trunk\TDGame\proj.win32\Release.win32\TDGame.exe ..\..\Arts\demo\游戏\ 2>nul

echo 拷贝脚本到demo目录
xcopy /e /q /y ..\..\trunk\TDGame\Resources\scripts\*.* ..\..\Arts\demo\游戏\scripts\ 2>nul
ping /n 3 127.0.1 >nul
goto :eof
