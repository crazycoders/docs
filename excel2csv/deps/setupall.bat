@echo off

cd /d %~dp0
echo %cd%

set hasPython=0
set has7z=0
echo %Path% | findstr "D:\Python27" >nul
if %errorlevel%==0 set hasPython=1
echo %Path% | findstr "D:\Program Files\7-Zip" >nul
if %errorlevel%==0 set has7z=1

if %hasPython%==1 (
if %has7z%==1 (
goto :step1
)
)

:step0
echo install 7z... DO NOT CLOSE!
call 7z920-x64.msi
echo 7z installed.

echo install python-2.7.6.msi... DO NOT CLOSE!
call python-2.7.6.msi
echo python-2.7.6.msi installed.

echo setup env...
cscript setupenv.vbs
set Path=%Path%;D:\Program Files\7-Zip;D:\Python27
echo %Path%
rem start setupall.bat 2
rem goto :eof

:step1
echo install setuptools... DO NOT CLOSE!
if not exist setuptools-2.2 7z x setuptools-2.2.7z
cd setuptools-2.2
python setup.py install
cd ..

:step2
echo install openpyxl... DO NOT CLOSE!
if not exist ericgazoni-openpyxl 7z x ericgazoni-openpyxl.7z
cd ericgazoni-openpyxl
python setup.py install
cd ..

:step3
echo install xmlwt... DO NOT CLOSE!
if not exist xlwt-0.7.5 7z x xlwt-0.7.5.7z
cd xlwt-0.7.5
python setup.py install
cd ..

echo Setupall success, Exiting... & ping /n 3 127.0.1 >nul

pause
goto :eof

