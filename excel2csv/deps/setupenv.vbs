'
' Copyright (c) 2015 Unknown Corporation., All Right Reserved.
'
PATH_OF_7Z="C:\Program Files\7-Zip"
PATH_OF_PYTHON="C:\Python27"

' set system env vars
Set sysEnv = WScript.CreateObject("WScript.Shell").Environment("User")  ' User

' set java and android sdk tools bin path
PATH=sysEnv.Item("PATH")
If 0 = InStr(PATH, PATH_OF_7Z) Then
 PATH = PATH & ";" & PATH_OF_7Z
 sysEnv.Item("PATH") = PATH
End If

If 0 = InStr(PATH, PATH_OF_PYTHON) Then
 PATH = PATH & ";" & PATH_OF_PYTHON
 sysEnv.Item("PATH") = PATH
End If

Set sysEnv = Nothing

'ends
