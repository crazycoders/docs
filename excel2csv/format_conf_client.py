# -*- coding: UTF-8 -*-
from openpyxl import load_workbook
from xlwt import Workbook
import os, fnmatch
import codecs

class format_conf:
    def __init__(self):
        self.type = "line"
        self.out_confs = []
    
def load_format_conf(sheet):
    ret = format_conf()
    for cells in sheet.rows:

        if cells[0].value != None and type(cells[0].value) != int:
            if ".xml" in cells[0].value or ".lua" in cells[0].value or ".ini" in cells[0].value or ".cfg" in cells[0].value or ".csv" in cells[0].value or ".json" in cells[0].value or ".font" in cells[0].value or ".xls" in cells[0].value:
                ret.out_confs.append(cells[0].value)
            else:
                return ret
        else:
            return ret
    return ret

def node_desc(name):
    return name.split('.', 1)[0]
    
def is_conf_node(desc, i):
    if (1 == desc[i].value):
        return True
    return False
    
def is_keyword_node(desc, i):
    if (2 == desc[i].value):
        return True
    return False

def is_multi_key_node(desc, i):
    if (3 == desc[i].value):
        return True
    return False

def is_number(value):
    result = True
    try:
        int(value)
    except:
        result = False
    if result == False:
        try:
            result = True
            float(value)
        except:
            result = False
    return result

def cell_value(c, t="int"):
    #if (c.is_date()):
    #    return c.value.strftime("%Y-%m-%d_%H:%M:%S")
    #el
    if c.value != None:
        result = True
        try:
            c.value = c.value.replace('\n', '')
        except:
            result = False
        IntValue = c.value
        return unicode(IntValue)
    else:
        if t == "string" or t == "string*":
            return ""
        else:
            return "0"
    
def cell_json_value(c):

    if (c.value):
        result = True
        try:
            c.value = c.value.replace('\n', '')
        except:
            result = False
        IntValue = c.value
        return unicode(IntValue)
    else:
        return ""
        
def xml_entry(c, d):
    return u"<%s>%s</%s>"%(d, c, d)
        
def to_xml_row_str(rows, line, titleLine, row):
    return "<%s>\n%s\n</%s>"%(
        'pack', 
        '\n'.join([xml_entry(cell_value(row[i]), node_desc(rows[titleLine][i].value)) for i in range(1,len(row)) if is_conf_node(rows[line], i)]), 
        'pack')

def to_ini_row_str(rows, line, titleLine, row):
    return '  '.join([cell_value(row[i]) for i in range(1,len(row)) if is_conf_node(rows[line], i)])

def to_csv_row_str(rows, line, titleLine, row, typeline):
    return_value = ""
    bFirst = True
    for i in range(1,len(row)):
        if is_conf_node(rows[line], i):
            cellValue = cell_value(row[i], rows[typeline][i].value).replace('"','""')
            if cellValue.find(",") >= 0 or cellValue.find('"') >=0:
                cellValue = '"' + cellValue + '"'
            if bFirst == True:
                return_value = return_value + cellValue
                bFirst = False
            else:
                return_value = return_value + ',' + cellValue
    return return_value


def to_lua_row_str(file_name, rows, line, titleLine, row):
    return_value = 'g_cfg["' + file_name + "_cfg"
    bFirst = True
    bHasMultiKey = False
    
    for i in range(1,len(row)):
        keyValue  = cell_value(rows[titleLine][i])
        cellValue = cell_value(row[i])
        
        if is_multi_key_node(rows[line], i):
            if False == bHasMultiKey:
                return_value = return_value + "$$"
                bHasMultiKey = True
            if is_number(cellValue) != True:
                cellValue = '"' + cellValue + '"'
            return_value = return_value + keyValue + "=" + cellValue + ","
        
        if is_keyword_node(rows[line], i):
            return_value = return_value + "_" + cellValue
            
        if is_conf_node(rows[line], i):
            if is_number(cellValue) != True:
                cellValue = '"' + cellValue + '"'
            
            if bFirst == True:
                return_value = return_value + '"]={' + keyValue + "=" + cellValue
                bFirst = False
            else:
                return_value = return_value + ',' + keyValue + "=" + cellValue
    return_value = return_value + "}"
    return  return_value


def to_json_row_str(file_name, rows, line, titleLine, row):
    return_value = '{'
    bFirst = True
    bHasMultiKey = False
    
    for i in range(1,len(row)):
        keyValue  = cell_value(rows[titleLine][i])
        cellValue = cell_json_value(row[i])
        bnumber = is_number(row[i].value) 
        if len(cellValue) <= 0:
            cellValue = "0"
       
        if len(cellValue) > 0:
            if i > 1:
                return_value = return_value + ","

            if bnumber or cellValue[0] == '{' or cellValue[0] == '[':
                return_value = return_value + '"' + keyValue + '":' + cellValue
            else:
                return_value = return_value + '"' + keyValue + '":' + '"' + cellValue + '"'
       
    
    return_value = return_value + "}"
    return  return_value

def to_xls_row_str(rows, line, cur_line, titleLine, row, worksheet):
    cur_column = 0
    for i in range(1,len(row)):
        if is_conf_node(rows[line], i):
            cellValue = row[i].value
            worksheet.write(cur_line, cur_column, cellValue)
            cur_column = cur_column + 1
    return

g_all_lua_string=""
g_all_font_string={}

def save_conf_file(filename, data):
    if data != "":
        filename = filename.replace('\\','/')
        f = codecs.open(filename, "w", "utf-8")
        f.write(data)
        f.close()

def ini_description(rows, line, titleLine):
    return '#'+'  '.join([node_desc(rows[titleLine][i].value) for i in range(1, len(rows[titleLine])) if is_conf_node(rows[line], i)])

def csv_description(rows, line, titleLine):
    return ','.join([node_desc(rows[titleLine][i].value) for i in range(1, len(rows[titleLine])) if is_conf_node(rows[line], i)])


def generate_conf_text(file_name, file_ext, conf_type, line, titleLine, rows, typeline):
    global g_all_lua_string
    global g_all_font_string

    totleLine = len(rows)
    for i in range(titleLine + 1, totleLine):
        if rows[i][1].value or rows[i][1].value == 0:
            continue
        else:
            totleLine = i
            break
    if (file_ext == ".xml"):
    
        xmlout = "<conf>\n%s\n</conf>"%('\n'.join([to_xml_row_str(rows, line, titleLine - 1, i) for i in rows[titleLine + 2:totleLine]]))
        xmlout = u"<?xml version='1.0' encoding='UTF-8' standalone='yes'?>\n" + xmlout
        return xmlout
    if (file_ext == ".ini" or file_ext == ".cfg"):
        iniout = "%s\n%s"%(ini_description(rows, line, titleLine), '\n'.join([to_ini_row_str(rows, line, titleLine, i) for i in rows[titleLine + 1:totleLine]]))
        return iniout
    if (file_ext == ".csv"):
        csvout = "%s\r\n%s"%(csv_description(rows, line, titleLine - 1), '\r\n'.join([to_csv_row_str(rows, line, titleLine, i, typeline) for i in rows[titleLine :totleLine]]))
        return csvout
    if (file_ext == ".lua"):
        luaout = "%s"%('\n'.join([to_lua_row_str(file_name, rows, line, titleLine, i) for i in rows[titleLine + 1:totleLine]]))
        if (g_all_lua_string != ""):
            g_all_lua_string = g_all_lua_string + "\n"
        g_all_lua_string = g_all_lua_string + luaout
        return luaout
    if (file_ext == ".json"):
        jsonout = "[" + "%s"%(",".join([to_json_row_str(file_name, rows, line, titleLine - 1, i) for i in rows[titleLine + 2:totleLine]]))
        jsonout = jsonout + "]"
        return jsonout
    if (file_ext == ".font"):
        fontout = "%s\n%s"%(ini_description(rows, line, titleLine), '\n'.join([to_ini_row_str(rows, line, titleLine, i) for i in rows[titleLine + 1:totleLine]]))
        if False == g_all_font_string.has_key(file_name):
            g_all_font_string[file_name] = fontout
        else:
            g_all_font_string[file_name] = g_all_font_string[file_name] + fontout
        return ""
    if (file_ext == ".xls"):
        wb = Workbook(encoding = 'ascii')
        worksheet = wb.add_sheet('Sheet1')
        cur_line = 0

        to_xls_row_str(rows, line, cur_line, titleLine, rows[titleLine-1], worksheet) 
        for i in rows[titleLine + 2:totleLine]:
            cur_line = cur_line + 1
            to_xls_row_str(rows, line, cur_line, titleLine, i, worksheet) 
            
        file_name = file_name.replace('\\','/')
        wb.save(file_name + ".xls")
        return ""
    return ""
        
def format_one_sheet(filename, sheet):
    fconf = None
    fconf = load_format_conf(sheet)
	
    #output conf files
    if (fconf == None or len(fconf.out_confs) <= 0):
        return
        
    # next row must be comment
    sheet_desc = sheet.rows[len(fconf.out_confs)]
    titleLine = len(fconf.out_confs);
    line = 0
    for fcode in fconf.out_confs:
        confname = fcode
        file_desc = os.path.splitext(confname)
        file_name = file_desc[0]
        file_ext  = file_desc[1]
        save_conf_file(confname, generate_conf_text(file_name, file_ext, fconf.type, line, titleLine+1, sheet.rows, len(fconf.out_confs) + 1))
        line = line + 1


def format_one_conf(filename):

    if (filename.startswith('./~')):
        return
  
    wb = load_workbook(filename = filename)
    print "now process xlsm file %s"%(filename)

    for i in wb.get_sheet_names():
        if False == ("$$" in i):
            #print "now process %s" %(i)
            format_one_sheet(filename, wb.get_sheet_by_name(i))


_DEBUG=False

if __name__ == '__main__':

    if _DEBUG == True:
        import pdb
        pdb.set_trace()
    g_all_lua_string = "g_cfg={}"
    g_all_font_string = {}
    rootpath = "./clientxls/"
    xlsm_pat = "*.xlsm"
    xlsx_pat = "*.xlsx"
    #if False == os.path.exists("server") :
        #os.mkdir("server")
    #if False == os.path.exists("tool") :
    #    os.mkdir("tool")
    if False == os.path.exists("client") :
        os.mkdir("client")
    #if False == os.path.exists("md5") :
    #    os.mkdir("md5")
    #if False == os.path.exists("zip") :
    #    os.mkdir("zip")
    for root,dirs,files in os.walk(rootpath):
        for filespath in files:
            if (rootpath != root):
                continue
            
            if (not fnmatch.fnmatch(filespath, xlsm_pat)) and (not fnmatch.fnmatch(filespath, xlsx_pat)):
                continue
                
            fullname = os.path.join(root, filespath)
            format_one_conf(fullname)
    #save_conf_file("game_cfg.lua", g_all_lua_string)
    
    for name in g_all_font_string:
        file_name = name + ".font"
        save_conf_file(file_name, g_all_font_string[name])
    #os.system("config.exe")
