@echo off

cd %~dp0

call pcode_autog.exe


move /y *.h ..\frameworks\runtime-src\Classes\inet\
move /y *.cpp ..\frameworks\runtime-src\Classes\inet\

move /y  pcode_autog_client_constants.lua ..\src\
move /y protocol_enc.lua ..\src\
move /y protocol_dec.lua ..\src\
move /y protocol_enums.lua ..\src\

goto :eof
