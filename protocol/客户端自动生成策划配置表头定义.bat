@echo off
@echo 正在生成配置头文件

call vcode_autog.exe 1 ../res/config
call vcode_autog.exe 2 ../res/config

move /y DataSourceAccessCSV.h ../frameworks/runtime-src/Classes/data/
move /y data_source_access_csv.lua ../src/

ping /n 2 127.0.1 >nul

pause
goto :eof
